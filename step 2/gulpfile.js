
const gulp = require ('gulp');
const sass = require ('gulp-sass');
const uglify = require ('gulp-uglify');
const clean = require ('gulp-clean');
const cleanCSS = require ('gulp-clean-css');
const concat = require ('gulp-concat');
const imageMin = require ('gulp-imagemin');
const prefix = require ('gulp-autoprefixer');
const jsMinify = require ('gulp-js-minify');
const browserSync = require ('browser-sync');


function cleanDist () {
    return gulp.src('./dist', {read: false})
        .pipe(clean());
}

function scss() {
    return gulp.src('./src/scss/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('style.min.css'))
        .pipe(prefix())
        .pipe(cleanCSS({
            level:2
        }))
        .pipe(gulp.dest('./dist/css'))
        .pipe(browserSync.reload({
            stream: true
        }))
}

function js() {
    return gulp.src('src/js/*.js')
        .pipe(concat('script.min.js'))
        .pipe(jsMinify())
        .pipe(uglify())
        .pipe(gulp.dest('./dist/js'));
}

function img() {
    return  gulp.src('src/img/**/*')
        .pipe(imageMin())
        .pipe(gulp.dest("./dist/img"));
}

function browserReload() {
    browserSync.init({
        server: {
            baseDir: '.'
        },
    })
}

function watch() {
        gulp.watch('./src/scss/**/*.scss', scss);
        gulp.watch('src/js/*.js', js);
        gulp.watch('src/img/**/*', img);
    }


gulp.task('cleanDist', cleanDist);
gulp.task('scss', scss);
gulp.task('js', js);
gulp.task('img', img);
gulp.task('watch', watch)

gulp.task('browserReload',browserReload);

gulp.task('build', gulp.series('cleanDist', gulp.parallel(scss, js, img)));

gulp.task('dev', gulp.series ('build', gulp.parallel(watch, browserReload)));


